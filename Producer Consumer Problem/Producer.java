import java.util.Queue;

public class Producer extends Thread{
	Queue<Integer> q;
	Producer(Queue<Integer> q){
		this.q=q;
	}
	public void run() {
		Integer i=q.size();
		while(q.size() < 5) {
			q.add(i);
			System.out.println("Produced: "+i);
			i++;
		}
		synchronized(q) {
			System.out.println("Produced all 5 units...");
			q.notifyAll();
		}
	}
}

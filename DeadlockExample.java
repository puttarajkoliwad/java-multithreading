class A extends Thread{
	B bobject;
	public synchronized void m1(B b) {
		System.out.println("ThreadA executing m1...");
		try {
			System.out.println("ThreadA sleeping...");
			Thread.sleep(1000);
		}catch(InterruptedException e) {e.printStackTrace();}
		b.last();
	}
	
	public synchronized void last() {
		System.out.println("This is A.last()...");
	}
	
	public void run() {
		this.m1(bobject);
	}
}

class B extends Thread{
	A aobject;
	public synchronized void m2(A a) {
		System.out.println("ThreadB executing m2...");
		try {
			System.out.println("ThreadB sleeping...");
			Thread.sleep(1000);
		}catch(InterruptedException e) {e.printStackTrace();}
		a.last();
	}
	
	public synchronized void last() {
		System.out.println("This is B.last()...");
	}
	
	public void run() {
		this.m2(aobject);
	}
}

public class DeadlockExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		A a= new A();
		B b= new B();
		a.bobject = b;
		b.aobject = a;
		System.out.println("Starting both threads...");
		a.start();
		b.start();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("ThreadA state: "+a.getState());
		System.out.println("ThreadB state: "+a.getState());
		try {
			a.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			b.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Program executed successfully...");
	}

}

class Runnable1 implements Runnable{
	public void run() {
		System.out.println("Pun: Runnable can't run without passing to Thread!");
	}
}


public class RunnableInterface {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Multithreading using Runnable Interface:");
		System.out.println("========================================\n");
		Thread t=new Thread(new Runnable1());
		t.start();
	}
}

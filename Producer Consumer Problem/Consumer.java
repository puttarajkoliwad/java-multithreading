import java.util.Queue;

public class Consumer extends Thread {
	Queue<Integer> q;
	Consumer(Queue<Integer> q){
		this.q=q;
	}
	public void run() {
		
		if(q.isEmpty()) {
			synchronized(q) {
				System.out.println("Consumer is waiting");
				try {
					q.wait(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		System.out.println("Consumer starting to consume...");
		while(!q.isEmpty()) {
			q.poll();
			System.out.println("Consumed: "+q.size());
		}
	}

}

import java.util.LinkedList;
import java.util.Queue;

public class ProducerConsumerDriver {

	public static void main(String[] args) {
		Queue<Integer> q= new LinkedList<Integer>();
		Producer p=new Producer(q);
		Consumer c= new Consumer(q);
		
		c.start();
		p.start();
	}

}

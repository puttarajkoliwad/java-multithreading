class Daemon extends Thread{
	public void run() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(int i=0; i<10; i++) {
			System.out.println("Hello: "+i);
		}
	}
}

public class DaemonThreads {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Daemon d=new Daemon();
		d.setDaemon(true);
		/* try {
			d.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} *///This piece should IDEALLY be after d.start()
		d.start();
		System.out.println("End of main...");
	}

}

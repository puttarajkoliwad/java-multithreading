class Thread1 extends Thread{
	public void run() {
		for(int i=0; i<5;i++) {
			System.out.println("Hi");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}

class Thread2 extends Thread{
	public void run() {
		for (int i = 0; i < 5; i++) {
			System.out.println("Hello");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}


public class ThreadClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Multithreading using Thread class:");
		System.out.println("==================================\n");
		Thread1 t1 = new Thread1();
		Thread2 t2= new Thread2();
		t1.start();
		try {
			Thread.sleep(100);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		t2.start();
	}

}
